﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Percentage_Management : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        SqlDataSource1.EnableViewState = true;
        GridView1.EnableViewState = true;
        GridView1.Enabled = true;
        GridView1.Visible = true;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        TextBox2.Visible = false;
        TextBox2.Text = "";
        Label6.Visible = false;
        
        Panel1.Visible = true;
        SqlDataSource1.EnableViewState = false;
        GridView1.EnableViewState = false;
        GridView1.Enabled = false;
        GridView1.Visible = false;

    }
    protected void Button3_Click1(object sender, EventArgs e)
    {
        TextBox2.Visible = false;
        TextBox2.Text = "";
        Label6.Visible = false;
        Panel1.Visible = true;
        SqlDataSource1.EnableViewState = false;
        GridView1.EnableViewState = false;
        GridView1.Enabled = false;
        GridView1.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        DropDownList1.SelectedIndex = 0;
        TextBox1.Text = "";
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox2.Visible = false;
            TextBox2.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;
            if (DropDownList1.SelectedIndex == 0 || TextBox1.Text.ToString().Equals(""))
            {
                if (DropDownList1.SelectedIndex == 0)
                {
                    Label4.Visible = true;
                }
                if (TextBox1.Text.ToString().Equals(""))
                {
                    Label5.Visible = true;
                }
            }

            else
            {
                float a = float.Parse(TextBox1.Text);
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("insert into Percentage values('" + DropDownList1.SelectedValue.ToString() + " '," +a + ")", cn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                Label6.Visible = true;
                Label6.Text = "One Record Inserted";
            }


        }
        catch (Exception c)
        {
            TextBox2.Visible = true;
            TextBox2.Text = c.ToString();
            Label6.Visible = true;
            Label6.Text = "Error !";
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }

    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox2.Visible = false;
            TextBox2.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if (DropDownList1.SelectedIndex == 0 || TextBox1.Text.ToString().Equals(""))
            {
                if (DropDownList1.SelectedIndex == 0)
                {
                    Label4.Visible = true;
                }
                if (TextBox1.Text.ToString().Equals(""))
                {
                    Label5.Visible = true;
                }
            }

            else
            {
                float a = float.Parse(TextBox1.Text);
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("update Percentage set [Percent] = "+a+" where Standard='"+DropDownList1.SelectedValue.ToString()+"'", cn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                Label6.Visible = true;
                Label6.Text = "One Record Updated";
            
            }


        }
        catch (Exception c)
        {
            TextBox2.Visible = true;
            TextBox2.Text = c.ToString();
            Label6.Visible = true;
            Label6.Text = "Error !";
        
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
   
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox2.Visible = false;
            TextBox2.Text = "";
            Label5.Visible = false;
            Label4.Visible = false;
            Label6.Visible = false;  
            if (DropDownList1.SelectedIndex == 0)
                {
                    Label4.Visible = true;
                }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("delete from Percentage where Standard='"+DropDownList1.SelectedValue.ToString()+"'", cn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

                Label6.Visible = true;
                Label6.Text = "One Record Deleted";
            
            }


        }
        catch (Exception c)
        {
            TextBox2.Visible = true;
            TextBox2.Text = c.ToString();
            Label6.Visible = true;
            Label6.Text = "Error !";
        
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
   
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox2.Visible = false;
            TextBox2.Text = "";
            Label4.Visible = false;
            Label6.Visible = false;
            Label5.Visible = false;
             
            cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            cmd = new SqlCommand("delete from Percentage", cn);
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();

            Label6.Visible = true;
            Label6.Text = "All Records Deleted";
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "Error !";
            TextBox2.Visible = true;
            TextBox2.Text = c.ToString();
        
        }
    }
}