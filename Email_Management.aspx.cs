﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Email_Management : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        TextBox1.Text = "";
        DropDownList1.SelectedIndex = 0;
        TextBox2.Text = "";
        TextBox2.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        GridView1.Visible = false;
        GridView1.Enabled = false;
        GridView1.EnableViewState = false;
        SqlDataSource1.EnableViewState = false;
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        TextBox1.Text = "";
        DropDownList1.SelectedIndex = 0;
        TextBox2.Text = "";
        TextBox2.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        GridView1.Visible = true;
        GridView1.Enabled = true;
        GridView1.EnableViewState = true;
        SqlDataSource1.EnableViewState = true;
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;       
            Label6.Visible = false;

            if (TextBox1.Text.Equals("") || DropDownList1.SelectedIndex == 0)
            {
                if (TextBox1.Text.Equals(""))
                {
                    Label5.Visible = true;
                }
                if (DropDownList1.SelectedIndex == 0)
                {
                    Label4.Visible = true; ;
                }
            }
            else 
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("insert into Email values('"+TextBox1.Text+"','"+DropDownList1.SelectedValue.ToString()+"') ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                { 
                Label6.Visible = true;
                Label6.Text = "One Row Inserted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally 
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if ( DropDownList1.SelectedIndex == 0)
            {
                Label4.Visible = true;   
            }
            else if (TextBox1.Text.Equals(""))
            {
                Label5.Visible = true;
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Update Email set Email='" + TextBox1.Text + "' where Selected ='" + DropDownList1.SelectedValue.ToString() + "' ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One Row Updated";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if (DropDownList1.SelectedIndex == 0)
            {
                Label4.Visible = true;
            }
            else if (TextBox1.Text != "")
            {
                Label6.Visible = true;
                Label6.Text = "No NEED To Fill Email For Deleting";
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("delete from Email where Selected ='" + DropDownList1.SelectedValue.ToString() + "' ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One Row Deleted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
   
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if (DropDownList1.SelectedIndex != 0)
            {
                Label6.Visible = true;
                Label6.Text = "No NEED To Select For Deleting All";
            }
            else if (TextBox1.Text != "")
            {
                Label6.Visible = true;
                Label6.Text = "No NEED To Fill Email For Deleting";
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("delete from Email", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "All Rows Deleted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            DropDownList1.SelectedIndex = 0;
        }
   
    }
}