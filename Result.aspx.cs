﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Net.Configuration;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Data;


public partial class Result : System.Web.UI.Page
{
    SqlConnection cn;
   SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {
        int count;
        Label3.Text = Session["M1"].ToString();
        Label5.Text = Session["M2"].ToString();
        Label7.Text = Session["M3"].ToString();
        int m1 = int.Parse(Session["M1"].ToString());
        int m2 = int.Parse(Session["M2"].ToString());
        int m3 = int.Parse(Session["M3"].ToString());
        if (m1>=6 && m2>=6 && m3>=6 )
        {
            Label8.Text = "You Have Cleared The Test";
            Label9.Text = "Your Result And HR Details Have Been Sent To Your Email ID";
            Label10.Text = "Thank You For Taking Part in Our Recruitment Process";
            cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            cmd = new SqlCommand("update Test set Test_Cleared='Yes' where ID=" + Session["uid"].ToString() + "", cn);
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Close();
            SqlConnection a1 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            SqlCommand b1 = new SqlCommand("update Register set Test_Cleared='Yes' where ID=" + Session["uid"].ToString() + "", a1);
            b1.Connection.Open();
            b1.ExecuteNonQuery();
            cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            cmd = new SqlCommand("select Name,Email_ID from Register where ID=" + int.Parse(Session["uid"].ToString()) + "", cn);
            cmd.Connection.Open();
            SqlDataReader dr1 = cmd.ExecuteReader();
            string a = ""; 
            if(dr1.Read())
            { a= dr1[1].ToString();}
            SqlConnection cn2 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            SqlCommand cmd2 = new SqlCommand("select Email_Address,Password from Default_Email_Address", cn2);
            cmd2.Connection.Open();
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.Read())
            {
                
                string b = dr2[0].ToString();
                string c = dr2[1].ToString();
                cmd2.Connection.Close();
                StringBuilder s = new StringBuilder();
                s.Append("Dear ");
                s.Append(dr1[0].ToString());
                s.Append(",");
                s.AppendLine();
                s.AppendLine();
                s.Append("Thanks For Giving Test At www.recuitment.com");
                s.AppendLine();
                s.AppendLine();
                s.Append("You Have Cleared The Test");
                s.AppendLine();
                s.Append("Your Test Result is");
                s.AppendLine();
                s.Append("Test1_Marks :" + Label3.Text);
                s.AppendLine();
                s.Append("Test2_Marks :" + Label5.Text);
                s.AppendLine();
                s.Append("Test3_Marks :" + Label7.Text);
                s.AppendLine();
                s.Append("HR Details Are : ");
                s.AppendLine();
                s.AppendLine("HR_Skype_ID         Start_Time              End_Time");
                SqlConnection cn4 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                SqlCommand cmd4 = new SqlCommand("select HR_Skype_ID,Start_Time,End_Time,ID from Video", cn4);
                cmd4.Connection.Open();
                SqlDataReader dr5;
                int i;
                dr5 = cmd4.ExecuteReader();
                while (dr5.Read())
                {
                    s.Append(dr5[0].ToString() + "      ");
                    s.Append(dr5[1].ToString() + "      ");
                    s.Append(dr5[2].ToString() + "      ");
                    s.AppendLine();
                     i = int.Parse(dr5[3].ToString());
                    dr5.Dispose();
                    dr5.Close();
                    cmd4.Dispose();
                    cn4.Dispose();
                    cn4 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                    cmd4 = new SqlCommand("select HR_Skype_ID,Start_Time,End_Time,ID from Video where ID>" + i + "", cn4);
                    cmd4.Connection.Open();
                    dr5 = cmd4.ExecuteReader();
                }
                s.Append("Connect With These HR On Skype For Your Interview On The Given Time");
                s.Append("All The Best For Interview");
                MailMessage objEmail = new MailMessage();

                objEmail.From = new MailAddress(b);
                objEmail.To.Add(a);
                objEmail.Subject = "You Have Cleared The Test ";

                objEmail.Body = s.ToString();


                SmtpClient mail = new SmtpClient();
                mail.Host = "smtp.gmail.com";
                mail.Port = 587;
                mail.Credentials = new System.Net.NetworkCredential(b, c);

                mail.EnableSsl = true;
                mail.Send(objEmail);

            }
        
        }
        else 
        {
            Label8.Text = "You Have Not Cleared The Test";
            Label9.Text = "Your Result  Has Been Sent To Your Email ID";
            Label10.Text = "Thank You For Taking Part in Our Recruitment Process";
            cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            cmd = new SqlCommand("update Test set Test_Cleared='No' where ID=" + Session["uid"].ToString() + "", cn);
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            cn.Close();
            SqlConnection a1 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            SqlCommand b1 = new SqlCommand("update Register set Test_Cleared='No' where ID=" + Session["uid"].ToString() + "", a1);
            b1.Connection.Open();
            b1.ExecuteNonQuery();
            
            cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            cmd = new SqlCommand("select Name,Email_ID from Register where ID=" + int.Parse(Session["uid"].ToString()) + "", cn);
            cmd.Connection.Open();
            SqlDataReader dr1 = cmd.ExecuteReader();
            string a = "";
            if (dr1.Read())
            { a = dr1[1].ToString(); }
            SqlConnection cn2 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
            SqlCommand cmd2 = new SqlCommand("select Email_Address,Password from Default_Email_Address", cn2);
            cmd2.Connection.Open();
            SqlDataReader dr2 = cmd2.ExecuteReader();
            if (dr2.Read())
            {

                string b = dr2[0].ToString();
                string c = dr2[1].ToString();
                cmd2.Connection.Close();
                StringBuilder s = new StringBuilder();
                s.Append("Dear ");
                s.Append(dr1[0].ToString());
                s.Append(",");
                s.AppendLine();
                s.AppendLine();
                s.Append("Thanks For Giving Test At www.recuitment.com");
                s.AppendLine();
                s.AppendLine();
                s.Append("You Have Not Cleared The Test");
                s.AppendLine();
                s.Append("Your Test Result is");
                s.AppendLine();
                s.Append("Test1_Marks :" + Label3.Text);
                s.AppendLine();
                s.Append("Test2_Marks :" + Label5.Text);
                s.AppendLine();
                s.Append("Test3_Marks :" + Label7.Text);
                s.AppendLine();
                s.Append("Best Of Luck For Your Future Don't Be Disappointed");
                MailMessage objEmail = new MailMessage();

                objEmail.From = new MailAddress(b);
                objEmail.To.Add(a);
                objEmail.Subject = "You Have Not Cleared The Test ";

                objEmail.Body = s.ToString();


                SmtpClient mail = new SmtpClient();
                mail.Host = "smtp.gmail.com";
                mail.Port = 587;
                mail.Credentials = new System.Net.NetworkCredential(b, c);

                mail.EnableSsl = true;
                mail.Send(objEmail);

            }
        
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    }
}