﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class Admin_Login : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    SqlDataReader dr;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Button10.Visible == true && Button11.Visible == true)
        {
            Button12.ToolTip = "Go Back To Welcome Page";
        }
        else 
        {
            Button12.ToolTip = "Go Back To Admin Page";
        
        }
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
            Label22.Visible = false;
            TextBox7.Visible = false;
            TextBox7.Text = "";
            Label22.Text = "";
            Label15.Visible = false;
            Label16.Visible = false;
            Label17.Visible = false;

            if (TextBox3.Text.ToString().Equals("") || TextBox4.Text.ToString().Equals("") || DropDownList3.SelectedIndex == 0)
            {
                if (TextBox3.Text.ToString().Equals(""))
                {
                    Label15.Visible = true;
                }
                if (TextBox4.Text.ToString().Equals(""))
                {
                    Label16.Visible = true;
                }
                if (DropDownList3.SelectedIndex == 0)
                {
                    Label17.Visible = true;
                }
            }

            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("select * from Admin where Username='" + TextBox3.Text + "' and User_Type='" + DropDownList3.SelectedValue.ToString() + "' ", cn);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    if (dr[1].ToString().Equals(TextBox4.Text.ToString()))
                    {
                        cmd.Connection.Close();
                        cmd = new SqlCommand("delete from Admin where Username='" + TextBox3.Text + "' and User_Type='" + DropDownList3.SelectedValue.ToString() + "' ", cn);
                        cmd.Connection.Open();
                        cmd.ExecuteNonQuery();
                        Panel5.Visible = false;
                        Panel2.Visible = true;
                        
                        Panel3.Visible = false;
                        DropDownList4.SelectedIndex = DropDownList3.SelectedIndex;
                    }
                    else
                    {
                        Label20.Visible = true;
                    }

                }
                else
                {
                    Label20.Visible = true;
                }


            }
        }
        catch (Exception c)
        {

        }
        finally
        {
            TextBox3.Text = "";
            TextBox4.Text = "";
            DropDownList3.SelectedIndex = 0;
        }
    
    }
    protected void Button9_Click(object sender, EventArgs e)
    {
        try
        {
            Label18.Visible = false;
           
            if (TextBox5.Text.ToString().Equals("") )
            {
                Label18.Visible = true;
            }

            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                    cmd = new SqlCommand("insert into Admin values('" + TextBox5.Text + "','" + TextBox6.Text + "', '"+DropDownList4.SelectedValue.ToString()+"') ", cn);
                cmd.Connection.Open();
                int k=cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Button10.Visible = false;
                    Button11.Visible = true;
                    Panel3.Visible = true;
                    Panel2.Visible = false;
                    Panel5.Visible = false;
                    Label11.Text = "Login Admin After Registering";
                    Label11.Visible = true;
                    Label12.Visible = false;
                    Label13.Visible = false;
                    Label14.Visible = false;
                    Label19.Visible = false;
                }
            }
        }
        catch (Exception c)
        {
            TextBox7.Visible = true;
            TextBox7.Text = c.ToString();
            Label22.Visible = true;
            Label22.Text = "! ERROR";
        }
        finally
        {
            TextBox5.Text = "";
            TextBox6.Text = "";
            
        }
                
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            Label12.Visible = false;
            Label13.Visible = false;
            Label14.Visible = false;

            if (TextBox1.Text.ToString().Equals("") || TextBox2.Text.ToString().Equals("") || DropDownList2.SelectedIndex == 0)
            {
                if (TextBox1.Text.ToString().Equals(""))
                {
                    Label12.Visible = true;
                }
                if (TextBox2.Text.ToString().Equals(""))
                {
                    Label13.Visible = true;
                }
                if (DropDownList2.SelectedIndex == 0)
                {
                    Label14.Visible = true;
                }
            }

            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("select * from Admin where Username='" + TextBox1.Text+ "' and User_Type='"+DropDownList2.SelectedValue.ToString()+"' ", cn);
                cmd.Connection.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    if (dr[1].ToString().Equals(TextBox2.Text.ToString()))
                    {
                       if(DropDownList2.SelectedIndex==1)
                       {
                           Server.Transfer("~/Test_Management.aspx");
                       }
                       else if(DropDownList2.SelectedIndex==2)
                       {
                           Server.Transfer("~/Percentage_Management.aspx");
                       }

                       else if (DropDownList2.SelectedIndex == 3)
                       {
                           Server.Transfer("~/Qualification_Management.aspx");
                       }
                       else if (DropDownList2.SelectedIndex == 4)
                       {
                           Server.Transfer("~/News_Management.aspx");
                       }
                       else if (DropDownList2.SelectedIndex == 5)
                       {
                           Server.Transfer("~/HR_Management.aspx");
                       }
                       else if (DropDownList2.SelectedIndex == 6)
                       {
                           Server.Transfer("~/MD.aspx");
                       }
                       else if (DropDownList2.SelectedIndex == 7)
                       {
                           Session.Add("User_Name", TextBox1.Text);
                           Server.Transfer("~/HR.aspx");
                       }
                       else if (DropDownList2.SelectedIndex == 8)
                       {
                           Server.Transfer("~/Email_Management.aspx");
                       }
                       else 
                       {
                           Server.Transfer("~/Default_Email_Management.aspx");
                       }
                    }
                    else
                    {
                        Label19.Visible = true;    
                    }

                }
                else
                {
                    Label19.Visible = true; 
                }


            }
        }
        catch (Exception c)
        {
            
        }
        finally
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            DropDownList2.SelectedIndex = 0;
        }
        
    }

    
    protected void Button10_Click(object sender, EventArgs e)
    {
        TextBox3.Text = "";
        TextBox4.Text = "";
        DropDownList3.SelectedIndex = 0;
        Panel5.Visible = true;
        Button11.Visible = false;
        Panel3.Visible = false;
        Panel2.Visible = false;

    }
    protected void Button11_Click(object sender, EventArgs e)
    {
        TextBox1.Text = "";
        TextBox2.Text = "";
        DropDownList2.SelectedIndex = 0;
        Panel3.Visible = true;
        Panel2.Visible = false;
        Panel5.Visible = false;
        Button10.Visible = false;
    }
    protected void Button12_Click(object sender, EventArgs e)
    {
        if (Button10.Visible == true && Button11.Visible == true)
        {
            Server.Transfer("~/Welcome_Page.aspx");
        }
        else if (Button10.Visible == true || Button11.Visible == true)
        {
            Server.Transfer("~/Admin_Login.aspx");        
        }
    }
    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button13_Click(object sender, EventArgs e)
    {
        
    }
}