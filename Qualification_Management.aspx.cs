﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Qualification_Management : System.Web.UI.Page
{
    int k;
    SqlConnection cn,cn1;
    SqlCommand cmd,cmd1;
    SqlDataReader dr;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        TextBox5.Text = "";
        TextBox5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;
        Label4.Visible = false;
        Label9.Visible = false;
        Panel1.Visible = false;
        GridView1.Visible = true;
        GridView1.Enabled = true;
        GridView1.EnableViewState = true;
        SqlDataSource1.EnableViewState = true;
    }
    protected void Button3_Click1(object sender, EventArgs e)
    {
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox4.Text = "";
        Label9.Visible = false;
        TextBox5.Text = "";
        TextBox5.Visible = false;
        Label4.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;
        Panel1.Visible = true; ;
        GridView1.Visible = false;
        GridView1.Enabled = false;
        GridView1.EnableViewState = false;
        SqlDataSource1.EnableViewState = false;
    
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox5.Visible = false;
            TextBox5.Text = "";
        
            Label9.Visible = false;
            Label4.Visible = false;
            Label7.Visible = false;
            Label6.Visible = false;
            if (TextBox2.Text.ToString().Equals("") || TextBox3.Text.ToString().Equals(""))
            {
                if (TextBox2.Text.ToString().Equals(""))
                {
                    Label4.Visible = true;
                }
                if (TextBox3.Text.ToString().Equals(""))
                {
                    Label7.Visible = true;
                } 
            }

            else
            {
                cn1 = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                   
                cmd1 = new SqlCommand("insert into Qualification values('" + TextBox2.Text + "','" + TextBox3.Text + "')", cn1);
                    cmd1.Connection.Open();
                    k = cmd1.ExecuteNonQuery();
                    Label6.Visible = true;
                    Label6.Text = "One Record Inserted";
                

            }


        }
        catch (Exception c)
        {
            TextBox5.Visible = true;
            Label6.Visible = true;
            Label6.Text = "Error !";
            TextBox5.Text = c.ToString();
        
        }
        finally
        {
            TextBox2.Text = "";
            TextBox3.Text = "";
            
        }

   
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox5.Visible = false;
            TextBox5.Text = "";
            Label9.Visible = false;
            Label4.Visible = false;
            Label7.Visible = false;
            Label6.Visible = false;
           
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand(" delete from Qualification ", cn);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                Label6.Visible = true;
                Label6.Text = "All Records Deleted";
        }
        catch (Exception c)
        {
            TextBox5.Visible = true;
            Label6.Visible = true;
            Label6.Text = "Error !";
            TextBox5.Text = c.ToString();
        }
        finally
        {
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
        }

    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox5.Visible = false;
            TextBox5.Text = "";
            Label9.Visible = false;
            Label4.Visible = false;
            Label7.Visible = false;
            Label6.Visible = false;
            if (TextBox4.Text.ToString().Equals(""))
            {
                Label9.Visible = true;
            }
           
            else
            {
                    cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                    cmd = new SqlCommand(" delete from Qualification where ID= "+int.Parse( TextBox4.Text.ToString())+"", cn);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    Label6.Visible = true;
                    Label6.Text = "Record Deleted";                
            }


        }
        catch (Exception c)
        {
            TextBox5.Visible = true;
            Label6.Visible = true;
            Label6.Text = "Error !";
            TextBox5.Text = c.ToString();
        }
        finally
        {
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
        }

    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox5.Visible = false;
            TextBox5.Text = "";
            Label9.Visible = false;
            Label4.Visible = false;
            Label7.Visible = false;
            Label6.Visible = false;
            if (TextBox4.Text.ToString().Equals(""))
            {
                Label9.Visible = true;
            }
            else if (TextBox2.Text.ToString().Equals("") && TextBox3.Text.ToString().Equals(""))
            {
                if (TextBox2.Text.ToString().Equals(""))
                {
                    Label4.Visible = true;
                }
                if (TextBox3.Text.ToString().Equals(""))
                {
                    Label7.Visible = true;
                }
            }

            else
            {
                if (TextBox2.Text.ToString() != "")
                {
                    cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                    cmd = new SqlCommand("update Qualification set Graduate='"+TextBox2.Text+"' where ID="+int.Parse(TextBox4.Text.ToString())+" ", cn);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    Label6.Visible = true;
                    Label6.Text = "Records Updated";
                }
                if (TextBox3.Text.ToString() != "")
                {
                    cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                    cmd = new SqlCommand("update Qualification set post_Graduate='" + TextBox3.Text + "' where ID=" + int.Parse(TextBox4.Text.ToString()) + " ", cn);
                    cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                    Label6.Visible = true;
                    Label6.Text = "Records Updated";
                
                }
            }


        }
        catch (Exception c)
        {
            TextBox5.Visible = true;
            Label6.Visible = true;
            Label6.Text = "Error !";
            TextBox5.Text = c.ToString();
        }
        finally
        {
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
        }

    }
}