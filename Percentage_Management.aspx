﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Percentage_Management.aspx.cs" Inherits="Percentage_Management" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head runat="server">
    <title></title>
</head>
<body background="Recruitment.jpg" onload="noBack()">
    <form id="form1" runat="server">
     <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="40pt" ForeColor="#0099FF" 
        Text="Percentage Criteria Maintenance Panel"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button1_Click" Text="Logout" />
    
    <br />
    <br />
    <br />
    <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" Height="31px" 
        Text="Insert , Update and Delete Percentage According To Standard" 
        Width="480px" onclick="Button3_Click1" />
    <br />
    <br />
    <asp:Panel ID="Panel1" runat="server" Height="261px" Visible="False" 
        Width="401px">
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label2" runat="server" Font-Italic="True" 
            Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" Text="Standard"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DropDownList1" runat="server" Font-Italic="True" 
            Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF">
            <asp:ListItem Selected="True">Select Standard</asp:ListItem>
            <asp:ListItem>10TH</asp:ListItem>
            <asp:ListItem>12TH</asp:ListItem>
            <asp:ListItem>Graduate</asp:ListItem>
            <asp:ListItem>Post Graduate</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Italic="False" 
            Font-Names="Calisto MT" Font-Size="10pt" ForeColor="#0099FF" Text="Select It !" 
            Visible="False"></asp:Label>
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Font-Italic="True" Font-Size="12pt" 
            ForeColor="#0099FF" Text="Percentage"></asp:Label>
        &nbsp;&nbsp;
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Italic="False" 
            Font-Names="Calisto MT" Font-Size="10pt" ForeColor="#0099FF" Text="Fill It !" 
            Visible="False"></asp:Label>
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button4" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Italic="False" Font-Names="Calisto MT" 
            Font-Size="10pt" ForeColor="#0099FF" Text="Insert" 
            ToolTip="Enter Standard and Percent To Insert" onclick="Button4_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button5" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="10pt" 
            ForeColor="#0099FF" Text="Update" 
            ToolTip="Choose The Standard You Want To Update" onclick="Button5_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="10pt" 
            ForeColor="#0099FF" Text="Delete" 
            ToolTip="Select The Standard You Want To Delete Entry" 
            onclick="Button6_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button7" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="10pt" 
            ForeColor="#0099FF" onclick="Button7_Click" Text="Delete All" />
        <br />
        <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
            Font-Size="14pt" ForeColor="#0099FF" Visible="False"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox2" runat="server" Font-Names="Calisto MT" 
            ForeColor="#0099FF" style="resize:none" Height="45px" TextMode="MultiLine" Visible="False" 
            Width="357px"></asp:TextBox>
    </asp:Panel>
     <br />
    <asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="18pt" 
        ForeColor="#0099FF" Height="31px" onclick="Button2_Click" 
        Text="View The Percent According To Standard" Width="400px" />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" Enabled="False" 
        EnableViewState="False" ForeColor="#0099FF" GridLines="None" Height="150px" 
        Visible="False" Width="394px" EmptyDataText="No Records" Font-Bold="True" 
         Font-Names="Calisto MT" Font-Size="12pt">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Standard" HeaderText="Standard" 
                SortExpression="Standard" />
            <asp:BoundField DataField="Percent" HeaderText="Percent" 
                SortExpression="Percent" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:recruitmentConnectionString %>" 
        EnableViewState="False" SelectCommand="SELECT * FROM [Percentage]">
    </asp:SqlDataSource>
   
    </form>
</body>
</html>
