﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test_Management.aspx.cs" Inherits="Test_Management" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>
<head runat="server">
    <title></title>
</head>
<body onload="noBack()" background="Recruitment.jpg">
    <form id="form1" runat="server">
    <p>
    <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="40pt" ForeColor="#0099FF" 
        Text="Test Maintenance Panel"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button1_Click" Text="Logout" 
            style="height: 30px" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
    <br />
    <br />
    <asp:Panel ID="Panel1" runat="server" Height="298px" Width="399px">
        <asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="14pt" ForeColor="#0099FF" 
            onclick="Button6_Click" Text="Instuction For Maintenance" Width="231px" />
        <br />
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" Text="Aptitude Test Maintenance" 
            BackColor="White" BorderColor="White" BorderStyle="None" 
            Font-Names="Calisto MT" Font-Size="15pt" ForeColor="#0099FF" 
            onclick="Button2_Click" Width="248px" />
        <br />
        <br />
        <br />
        <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="15pt" ForeColor="#0099FF" 
            onclick="Button3_Click" Text="Logical Test Maintenance" Width="225px" />
        <br />
        <br />
        <br />
        <asp:Button ID="Button4" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="15pt" ForeColor="#0099FF" 
            onclick="Button4_Click" Text="Technical Test Maintenance" Width="248px" />
    </asp:Panel>
    </form>
</body>
</html>
