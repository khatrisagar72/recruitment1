﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin_Login.aspx.cs" Inherits="Admin_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head runat="server">
    <title></title>
    <style type="text/css">
        #form1
        {
            height: 1076px;
        }
    </style>
</head>
<body background="Recruitment.jpg" onload="noBack()">
    <form id="form1" runat="server" clientidmode="Static">
    <div>
    
    </div>
    <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="40pt" ForeColor="#0099FF" 
        Text="Welcome Admin To Maintain Website"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button12" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button12_Click" Text="Back" />
    <p>
        &nbsp;</p>
    <asp:Panel ID="Panel1" runat="server" Height="941px" Width="838px">
        <asp:Button ID="Button11" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="25pt" 
            ForeColor="#0099FF" onclick="Button11_Click" Text="Login Admin" 
            Width="185px" Font-Strikeout="False" />
        <br />
        <asp:Panel ID="Panel3" runat="server" Height="222px" ViewStateMode="Enabled" 
            Visible="False" Width="393px" ClientIDMode="Static">
            <asp:Label ID="Label11" runat="server" Font-Italic="True" 
                Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" Text="Label" 
                Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" ForeColor="#0099FF" Text="User_Name"></asp:Label>
            &nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" ForeColor="#0099FF" Text="Password"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <asp:Label ID="Label13" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" ForeColor="#0099FF" Text="User_Type"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList2" runat="server" ForeColor="#0099FF" 
                Height="22px" Width="157px" 
                onselectedindexchanged="DropDownList2_SelectedIndexChanged">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Test_Management</asp:ListItem>
                <asp:ListItem>Percent_Management</asp:ListItem>
                <asp:ListItem>Qualification_Management</asp:ListItem>
                <asp:ListItem>News_Management</asp:ListItem>
                <asp:ListItem>HR_Management</asp:ListItem>
                <asp:ListItem>MD</asp:ListItem>
                <asp:ListItem>HR</asp:ListItem>
                <asp:ListItem>Email_Management</asp:ListItem>
                <asp:ListItem>Default_Email_Add_Management</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Select An Option !" Visible="False"></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
                BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="12pt" 
                ForeColor="#0099FF" onclick="Button6_Click" Text="Login" />
            <br />
            <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Wrong User Name or Password" 
                Visible="False"></asp:Label>
            <br />
            <br />
            <br />
            <br />
        </asp:Panel>
        <br />
        <asp:Button ID="Button10" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="25pt" 
            ForeColor="#0099FF" onclick="Button10_Click" Text="Register New Admin" 
            Width="269px" ViewStateMode="Enabled" />
        <br />
        <br />
        <asp:Panel ID="Panel5" runat="server" Height="230px" ViewStateMode="Enabled" 
            Visible="False" Width="371px" ClientIDMode="Static">
            <asp:Label ID="Label21" runat="server" Font-Italic="True" 
                Font-Names="Calisto MT" Font-Size="13pt" ForeColor="#0099FF" 
                Text="Login As Old Admin"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label5" runat="server" ForeColor="#0099FF" Text="User_Name"></asp:Label>
            &nbsp;
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label6" runat="server" ForeColor="#0099FF" Text="Password"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox4" runat="server" TextMode="Password"></asp:TextBox>
            <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" ForeColor="#0099FF" Text="User_Type"></asp:Label>
            &nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList3" runat="server" ForeColor="#0099FF" 
                Height="22px" Width="157px">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Test_Management</asp:ListItem>
                <asp:ListItem>Percent_Management</asp:ListItem>
                <asp:ListItem>Qualification_Management</asp:ListItem>
           <asp:ListItem>News_Management</asp:ListItem>
                <asp:ListItem>HR_Management</asp:ListItem>
                <asp:ListItem>MD</asp:ListItem>
                <asp:ListItem>HR</asp:ListItem>
             <asp:ListItem>Email_Management</asp:ListItem>
               <asp:ListItem>Default_Email_Add_Management</asp:ListItem>
            
                        </asp:DropDownList>
            <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            &nbsp;
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button7" runat="server" BackColor="White" BorderColor="White" 
                BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="12pt" 
                ForeColor="#0099FF" onclick="Button7_Click" Text="Login" />
            <br />
            <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Wrong User Name or Password" 
                Visible="False"></asp:Label>
            <br />
        </asp:Panel>
        <br />
        <asp:Panel ID="Panel2" runat="server" Height="232px" ViewStateMode="Enabled" 
            Visible="False" Width="370px" ClientIDMode="Static">
            <asp:Label ID="Label8" runat="server" ForeColor="#0099FF" Text="User_Name"></asp:Label>
            &nbsp;
            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="10pt" ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label9" runat="server" ForeColor="#0099FF" Text="Password"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox6" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label10" runat="server" ForeColor="#0099FF" Text="User_Type"></asp:Label>
            &nbsp;&nbsp;
            <asp:DropDownList ID="DropDownList4" runat="server" Enabled="False" 
                ForeColor="#0099FF" Height="22px" Width="157px">
                <asp:ListItem Selected="True">Select</asp:ListItem>
                <asp:ListItem>Test_Management</asp:ListItem>
                <asp:ListItem>Percent_Management</asp:ListItem>
                <asp:ListItem>Qualification_Management</asp:ListItem>
            <asp:ListItem>News_Management</asp:ListItem>
                <asp:ListItem>HR_Management</asp:ListItem>
                <asp:ListItem>MD</asp:ListItem>    
                <asp:ListItem>HR</asp:ListItem>
             <asp:ListItem>Email_Management</asp:ListItem>
               <asp:ListItem>Default_Email_Add_Management</asp:ListItem>
           
            </asp:DropDownList>
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button9" runat="server" BackColor="White" BorderColor="White" 
                BorderStyle="None" Font-Italic="True" Font-Names="Calisto MT" Font-Size="12pt" 
                ForeColor="#0099FF" onclick="Button9_Click" Text="Register" Width="59px" />
            <br />
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
                Font-Size="14pt" ForeColor="#0099FF" Text="Label" Visible="False"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox7" runat="server" ForeColor="#0099FF" Height="44px" 
                TextMode="MultiLine" Visible="False" Width="253px"></asp:TextBox>
        </asp:Panel>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        &nbsp;&nbsp;&nbsp;
        <br />
        <br />
    </asp:Panel>
    </form>
</body>
</html>
