﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Instruction.aspx.cs" Inherits="Instruction" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            text-decoration: underline;
        }
    </style>
</head>
<body background="Recruitment.jpg" bgcolor="White">
    <form id="form1" runat="server">
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
        Font-Size="14pt" Font-Underline="True" 
        
        Text="Instruction For Inserting , Updating And Deleting Questions From The Database Of Tests Are :" 
        ForeColor="#0099FF"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
        onclick="Button3_Click" Text="Back" ForeColor="#0099FF" />
    <br />
    <br />
    <br />
    <asp:Panel ID="Panel1" runat="server" BackColor="White" Font-Bold="True" 
        Font-Names="Calisto MT" Font-Size="12pt" Height="379px" Width="543px" 
        ForeColor="#0099FF">
        <span class="style1">Instructions For Inserting Questions in The Database</span> 
        :-<br class="style1" />
        <br />
        1. Keep The Id Of The Questions Being Inserted Sequential.<br /> 2. Check The ID 
        Of Questions In the database from the Table given to you.<br />
        <br />
        <span class="style1">Instructions For Updating Questions in The Database</span> 
        :-<br class="style1" />
        <br />
        1. While Updating The Question in The Database first Write The Id Of the<br /> 
        question you want to update than any of its properties you want to update.<br />
        <br />
        2.You Can Update Every Attribute Of The Question simultaneously.<br />
        <br />
        <span class="style1">Instructions For Deleting Questions in The Database</span> 
        :-<br class="style1" />
        <br />
        1. When you want to delete a question from database you can choose the Id<br /> 
        and than fill all the fields with a new question than it will replace the 
        question
        <br />
        in the database so that the ID can be sequential after deleting also.</asp:Panel>
    </form>
</body>
</html>
