﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Email_Management.aspx.cs" Inherits="Email_Management" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head runat="server">
    <title></title>
</head>
<body background="Recruitment.jpg" onload="noBack()">
    <form id="form1" runat="server">
    <div>

    <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="38pt" ForeColor="#0099FF" 
        Text="Email Management Panel"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button1_Click" Text="Logout" />
    
    </div>
    <div>
    
        <br />
    
    </div>
    <p>
        <asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
            ForeColor="#0099FF" onclick="Button2_Click" 
            Text="Insert,Update And Delete Email For Selected And Not Selected Applicant" 
            Width="527px" />
    </p>
    <asp:Panel ID="Panel1" runat="server" Height="256px" Visible="False" 
        Width="395px">
        <asp:Label ID="Label2" runat="server" Text="Email :" Font-Names="Calisto MT" 
            ForeColor="#0099FF"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox1" runat="server" Font-Names="Calisto MT" 
            ForeColor="#0099FF" Height="57px" style="resize:none" TextMode="MultiLine" Width="296px"></asp:TextBox>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="#0099FF" 
            Text="Fill IT !" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Font-Names="Calisto MT" 
            ForeColor="#0099FF" Text="Selected :"></asp:Label>
        &nbsp;
        <asp:DropDownList ID="DropDownList1" runat="server" Font-Names="Calisto MT" 
            Font-Size="12pt" ForeColor="#0099FF" Height="27px" Width="129px">
            <asp:ListItem Selected="True">Select</asp:ListItem>
            <asp:ListItem>Yes</asp:ListItem>
            <asp:ListItem>No</asp:ListItem>
        </asp:DropDownList>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
            ForeColor="#0099FF" Text="Select IT !" Visible="False"></asp:Label>
        <br />
        <br />
        &nbsp;&nbsp;
        <asp:Button ID="Button4" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button4_Click" Text="Insert" Width="49px" />
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button5" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button5_Click" Text="Update" Width="58px" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button6_Click" Text="Delete" Width="49px" />
        &nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button7" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button7_Click" Text="Delete All" Width="75px" />
        <br />
        <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="#0099FF" 
            Text="Label" Visible="False"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox2" runat="server" ForeColor="#0099FF" Height="46px" 
            TextMode="MultiLine" Visible="False" Width="271px" style="resize:none"></asp:TextBox>
    </asp:Panel>
    <br />
    <br />
    <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
        onclick="Button3_Click" 
        Text="View Emails For Selected And Not Selected Applicant's" Width="392px" />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" EmptyDataText="No Records" 
        Enabled="False" EnableViewState="False" Font-Names="Calisto MT" 
        Font-Size="12pt" ForeColor="#0099FF" GridLines="None" Visible="False" 
        Width="390px">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="Selected" HeaderText="Selected" 
                SortExpression="Selected" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:recruitmentConnectionString %>" 
        EnableViewState="False" SelectCommand="SELECT * FROM [Email]">
    </asp:SqlDataSource>
    </form>
</body>
</html>
