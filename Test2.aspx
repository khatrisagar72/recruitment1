﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test2.aspx.cs" Inherits="Test2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head id="Head1" runat="server">
    <title></title>
</head>
<body onload="noBack()" background="Recruitment.jpg">
    <form id="form1" runat="server" background="Recruitment.jpg" 
    clientidmode="Inherit">
    &nbsp;<asp:Label ID="Label3" runat="server" Text="Logical Test " Font-Bold="True" 
        Font-Size="35pt" ForeColor="#0099FF"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="90" ClientIDMode="Static">
    </asp:ScriptManager>
       <asp:Timer ID="Timer1" runat="server" Interval="1000" 
        ontick="Timer1_Tick" oninit="Page_Load" 
        ViewStateMode="Enabled">
  </asp:Timer>
<asp:Label ID="Label2" runat="server" Text="Time Remaining : " Font-Bold="True" 
        Font-Size="16pt" ForeColor="#0099FF"></asp:Label>
    &nbsp;<asp:Label ID="Label1" runat="server"  
        Font-Names="Calisto MT" Font-Size="15pt" ForeColor="#0099FF"></asp:Label>
          </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Init" />
        </Triggers>
    </asp:UpdatePanel>

    <br />

    <br />
    
    <asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" ClientIDMode="Static" Font-Names="Calisto MT" 
        Font-Size="15pt" ForeColor="#0099FF" onclick="Button2_Click" Text="Show Test" />
    <br />
    <br />
    <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="548px" 
        ViewStateMode="Enabled">
        <asp:TextBox ID="TextBox1" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" style="resize:none" ReadOnly="True" TextMode="MultiLine" 
            Width="540px" AutoPostBack="True">Q1.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel2" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel2" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox2" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" style="resize:none" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q2.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel3" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList2" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel3" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox3" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" style="resize:none" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q3.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel4" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList3" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel4" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox4" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" style="resize:none" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q4.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel5" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList4" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel5" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox5" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" style="resize:none" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q5.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel6" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList5" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel6" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox6" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" style="resize:none" TextMode="MultiLine" 
            Width="541" AutoPostBack="True">Q6.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel7" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList6" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
     <asp:Panel ID="Panel7" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox7" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" style="resize:none" 
            Width="541px" AutoPostBack="True">Q7.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel8" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList7" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel8" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox8" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt"  style="resize:none"
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q8.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel9" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList8" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel9" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox9" runat="server" BorderStyle="Solid"  style="resize:none"
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt" 
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q9.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel10" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList9" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
     <asp:Panel ID="Panel10" runat="server" BorderStyle="Solid" ClientIDMode="Static" 
        ForeColor="#0099FF" Height="221px" Visible="False" Width="547px">
        <asp:TextBox ID="TextBox10" runat="server" BorderStyle="Solid" 
            ClientIDMode="Static" Font-Names="Calisto MT" Font-Size="13pt"  style="resize:none"
            ForeColor="#0099FF" Height="78px" ReadOnly="True" TextMode="MultiLine" 
            Width="541px" AutoPostBack="True">Q10.</asp:TextBox>
        <br /><asp:UpdatePanel ID="UpdatePanel11" runat="server" 
        RenderMode="Inline" ViewStateMode="Enabled">
        <ContentTemplate>
        &nbsp;<asp:RadioButtonList ID="RadioButtonList10" runat="server" AutoPostBack="True"
            ClientIDMode="Static">
        </asp:RadioButtonList></ContentTemplate></asp:UpdatePanel>
    </asp:Panel>
    <br />
    <br />
    <br />
    <br />
   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="16pt" 
        ForeColor="#0099FF" Height="30px" onclick="Button1_Click" Text="Next Test" 
        ClientIDMode="Static" ViewStateMode="Enabled" />
    <br />
     
   
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </form>
</body>
</html>
