﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Default_Email_Management : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label7.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if (TextBox1.Text.Equals("") || TextBox3.Text.Equals(""))
            {
                if (TextBox1.Text.Equals(""))
                {
                    Label5.Visible = true;
                }
                if (TextBox3.Text.Equals(""))
                {
                    Label7.Visible = true; ;
                }
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("insert into Default_Email_Address values('" + TextBox1.Text + "','" + TextBox3.Text + "') ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One Row Inserted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            TextBox3.Text = "";
        }
    
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label7.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;

            if (TextBox1.Text.Equals(""))
            {
                Label5.Visible = true;
                Label6.Visible = true;
                Label6.Text = "Fill Email Address If You Want To Update Password";
            }
            else if (TextBox3.Text.Equals(""))
            {
                Label7.Visible = true;
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Update Default_Email_Address set Password='" + TextBox3.Text + "' where Email_Address ='" + TextBox1.Text + "' ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One Row Updated";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            TextBox3.Text = "";
        }
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label7.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;
            if (TextBox1.Text.Equals(""))
            {
                Label5.Visible = true;
            }
            else if (TextBox3.Text != "")
            {
                Label6.Visible = true;
                Label6.Text = "No Need To Fill Password When You Are Deleting Email Address";
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Delete from Default_Email_Address where Email_Address ='" + TextBox1.Text + "' ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One Row Deleted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            TextBox3.Text = "";
        }
    
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
            Label6.Text = "";
            Label7.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;
            if (TextBox1.Text != "")
            {
                Label5.Visible = true;
                Label6.Visible = true;
                Label6.Text = "No Need To Fill Email Address when Deleting All Records";            
            }
            else if (TextBox3.Text != "")
            {
                Label6.Visible = true;
                Label6.Text = "No Need To Fill Password When You Are Deleting Email Address";
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Delete from Default_Email_Address", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "All Rows Deleted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox2.Text = c.ToString();
            TextBox2.Visible = true;
        }
        finally
        {
            TextBox1.Text = "";
            TextBox3.Text = "";
        }
    
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox2.Visible = false;
        Label7.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        GridView1.Visible = true;
        GridView1.Enabled = true;
        GridView1.EnableViewState = true;
        SqlDataSource1.EnableViewState = true;
        TextBox3.Text = "";
       
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
        TextBox1.Text = "";
        TextBox2.Text = "";
        TextBox3.Text = "";
        TextBox2.Visible = false;
        Label5.Visible = false;
        Label7.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        GridView1.Visible = false;
        GridView1.Enabled = false;
        GridView1.EnableViewState = false;
        SqlDataSource1.EnableViewState = false;
    
    }
}