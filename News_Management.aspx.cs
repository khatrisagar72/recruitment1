﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class News_Management : System.Web.UI.Page
{
    SqlConnection cn;
    SqlCommand cmd;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Welcome_Page.aspx");
    
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        GridView1.Visible = true;
        GridView1.Enabled = true;
        GridView1.EnableViewState = true;
        SqlDataSource1.EnableViewState = true;
        Label5.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        TextBox3.Text = "";
        TextBox3.Visible = false;

    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        TextBox1.Text = "";
        TextBox2.Text = "";
        Panel1.Visible = true;
        SqlDataSource1.EnableViewState = false;
        GridView1.Enabled = false;
        GridView1.EnableViewState = false;
        GridView1.Visible = false;
        Label6.Visible = false;
        Label6.Text = "";
        TextBox3.Text = "";
        TextBox3.Visible = false;
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            Label5.Visible = false;
            Label6.Visible = false;
            TextBox3.Text = "";
            TextBox3.Visible = false;
            if (TextBox1.Text.ToString().Equals(""))
            {
                Label5.Visible = true;
            }
            else if (TextBox2.Text.ToString() != "")
            {
                Label6.Visible = true;
                Label6.Text = "Dont Fill ID For Inserting News";
                TextBox2.Text = "";
            }
            else 
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("insert into News values('"+TextBox1.Text+"')", cn);
                cmd.Connection.Open();
            int k=cmd.ExecuteNonQuery();
            if (k > 0)
            {
                Label6.Visible = true;
                Label6.Text = "One News Inserted";
            }
            }
        }
        catch(Exception c) 
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox3.Visible = true;
            TextBox3.Text = c.ToString();
        }
        finally 
        {
            TextBox1.Text = "";
            TextBox3.Text = "";
        }
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            Label5.Visible = false;
            Label6.Visible = false;
            TextBox3.Text = "";
            TextBox3.Visible = false;
            if (TextBox2.Text.ToString().Equals("") || TextBox1.Text.ToString().Equals(""))
            {
                if(TextBox2.Text.ToString().Equals(""))
                {
                Label6.Visible = true;
                Label6.Text = "Enter ID !";
                }
                if (TextBox1.Text.ToString().Equals(""))
                {
                    Label5.Visible = true;
                }
            
            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Update News set News = '"+TextBox1.Text+"' where Id= "+TextBox2.Text+" ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One News Updated";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox3.Visible = true;
            TextBox3.Text = c.ToString();
        }
        finally
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
        }
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        try
        {
            Label5.Visible = false;
            Label6.Visible = false;
            TextBox3.Text = "";
            TextBox3.Visible = false;
            if (TextBox2.Text.ToString().Equals("") || TextBox1.Text.ToString().Equals(""))
            {
                if (TextBox2.Text.ToString().Equals(""))
                {
                    Label6.Visible = true;
                    Label6.Text = "Enter ID !";
                }
                if (TextBox1.Text.ToString().Equals(""))
                {
                    Label5.Visible = true;
                }

            }
            else
            {
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("Update News set News = '" + TextBox1.Text + "' where Id= " + TextBox2.Text + " ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "One News Deleted And One News Inserted";
                }
            }
        }
        catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox3.Visible = true;
            TextBox3.Text = c.ToString();
        }
        finally
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
        }
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        try
        {
        Label5.Visible = false;
            Label6.Visible = false;
            TextBox3.Text = "";
            TextBox3.Visible = false;
                cn = new SqlConnection("server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True");
                cmd = new SqlCommand("delete from News ", cn);
                cmd.Connection.Open();
                int k = cmd.ExecuteNonQuery();
                if (k > 0)
                {
                    Label6.Visible = true;
                    Label6.Text = "All Rows Deleted";
                }
            
    }    
    catch (Exception c)
        {
            Label6.Visible = true;
            Label6.Text = "ERROR !";
            TextBox3.Visible = true;
            TextBox3.Text = c.ToString();
        }
        finally
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
        }
}    
    protected void Button8_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/News_Instruction.aspx");
    }
    
}