﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="News_Management.aspx.cs" Inherits="News_Management" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head runat="server">
    <title></title>
</head>
<body background="Recruitment.jpg" onload="noBack()">
    <form id="form1" runat="server">
    <div>
    
    <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="38pt" ForeColor="#0099FF" 
        Text="News Management Panel"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button1_Click" Text="Logout" />
    
       
    </div>
    <br />
    <br />
    <br />
    <asp:Button ID="Button8" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
        ForeColor="#0099FF" onclick="Button8_Click" 
        Text="Instruction For Inserting , Updating And Deleting News" Width="410px" />
    <br />
    <br />
    <br />
    <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="16pt" 
        ForeColor="#0099FF" Text="Insert , Update And Delete News " Width="332px" 
        onclick="Button3_Click" />
    <br />
    <br />
    <asp:Panel ID="Panel1" runat="server" Height="254px" Visible="False" 
        Width="388px">
        <asp:Label ID="Label2" runat="server" Text="News" Font-Bold="True" 
            Font-Names="Calisto MT" Font-Size="15pt" ForeColor="#0099FF"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox1" runat="server" Height="56px" TextMode="MultiLine" 
            Width="263px" style="resize:none"></asp:TextBox>
        <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Names="Calisto MT" 
            ForeColor="#0099FF" Text="Fill IT !" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
            Font-Size="15pt" ForeColor="#0099FF" Text="ID"></asp:Label>
        &nbsp;<asp:TextBox ID="TextBox2" runat="server" Width="80px"></asp:TextBox>
        &nbsp;&nbsp;<asp:Label ID="Label4" runat="server" Font-Bold="False" 
            Font-Names="Calisto MT" Font-Size="10pt" ForeColor="#0099FF" 
            Text="Fill ID When Updating And Deleting"></asp:Label>
        <br />
        <br />
        <asp:Button ID="Button4" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button4_Click" Text="Insert" />
        &nbsp;
        <asp:Button ID="Button5" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button5_Click" Text="Update" ToolTip="Fill ID When Updating" />
&nbsp;
        <asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button6_Click" Text="Delete " 
            ToolTip="Replace Old  News By New News" />
&nbsp;
        <asp:Button ID="Button7" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" 
            onclick="Button7_Click" Text="Delete All" />
        <br />
        <asp:Label ID="Label6" runat="server" Font-Bold="False" Font-Names="Calisto MT" 
            ForeColor="#0099FF" Visible="False"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox3" runat="server" Height="41px" 
            style="margin-bottom: 0px" TextMode="MultiLine" Visible="False" Width="287px"></asp:TextBox>
        <br />
    </asp:Panel>
    <br />
    <asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="14pt" 
        ForeColor="#0099FF" onclick="Button2_Click" Text="View Id's According To News" 
        Width="280px" Height="31px" />
    <br />
    <br />
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataKeyNames="Id" DataSourceID="SqlDataSource1" 
        EmptyDataText="No Records" Enabled="False" EnableViewState="False" 
        Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" GridLines="None" 
        Visible="False" Width="333px" Height="160px">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="News" HeaderText="News" SortExpression="News" />
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" 
                ReadOnly="True" SortExpression="Id" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:recruitmentConnectionString %>" 
        SelectCommand="SELECT * FROM [News]"></asp:SqlDataSource>
    </form>
</body>
</html>
