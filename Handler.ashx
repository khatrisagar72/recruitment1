﻿<%@ WebHandler Language="C#" Class="Handler" %>
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data.SqlClient;

public class Handler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["ID"] != null)
        {
            // context.Response.Write(context.Request.QueryString["id"]);
            string dbcon = "server=Sagar-pc\\SqlExpress;database=recruitment;Integrated Security=True";
            ;//ConfigurationManager.ConnectionStrings["payal"].ConnectionString;
            SqlConnection con = new SqlConnection(dbcon);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select image from Register where ID=@ID", con);
            cmd.Parameters.AddWithValue("@ID", context.Request.QueryString["id"].ToString());
            SqlDataReader dr = cmd.ExecuteReader();
            dr.Read();
            context.Response.BinaryWrite((byte[])dr["image"]);
            dr.Close();
            con.Close();
        }
        else
        {
            context.Response.Write("No Image Found");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
