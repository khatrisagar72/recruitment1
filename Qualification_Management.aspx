﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Qualification_Management.aspx.cs" Inherits="Qualification_Management" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript">
    window.history.forward();
    function noBack() { window.history.forward(); }
</script>

<head runat="server">
    <title></title>
</head>
<body background="Recruitment.jpg" onload="noBack()">
    <form id="form1" runat="server">
    <div>
    
    <asp:Label ID="Label1" runat="server" Font-Italic="True" 
        Font-Names="Calisto MT" Font-Size="38pt" ForeColor="#0099FF" 
        Text="Qualification Criteria Maintanence Panel"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="15pt" 
        ForeColor="#0099FF" onclick="Button1_Click" Text="Logout" />
    
    </div>
    <p>
        &nbsp;</p>
    <p>
    <asp:Button ID="Button3" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Italic="False" Font-Names="Calisto MT" Font-Size="13pt" 
        ForeColor="#0099FF" Height="31px" 
        Text="Insert , Update and Delete Qualification According To Company" 
        Width="468px" onclick="Button3_Click1" Font-Bold="False" />
    </p>
    <asp:Panel ID="Panel1" runat="server" Height="271px" HorizontalAlign="Left" 
        Visible="False" Width="591px">
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
            Font-Size="10pt" ForeColor="#0099FF" Text="Fill ID Only When Using Update"></asp:Label>
        <br />
        &nbsp;<asp:Label ID="Label2" runat="server" Font-Italic="False" 
            Font-Names="Calisto MT" Font-Size="11pt" ForeColor="#0099FF" 
            Text="Graduate Qualification"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Italic="False" 
            Font-Names="Calisto MT" Font-Size="10pt" ForeColor="#0099FF" Text="Fill It !" 
            Visible="False"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label8" runat="server" Font-Bold="True" 
            Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" Text="ID"></asp:Label>
        &nbsp;<asp:TextBox ID="TextBox4" runat="server" Width="94px"></asp:TextBox>
        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Italic="False" 
            Font-Names="Calisto MT" ForeColor="#0099FF" Text="Fill IT!" 
            Visible="False"></asp:Label>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />&nbsp;<asp:Label ID="Label3" runat="server" Font-Italic="False" Font-Size="11pt" 
            ForeColor="#0099FF" Text="Post_Graduate Qualification"></asp:Label>
        &nbsp;&nbsp;<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Italic="False" 
            Font-Names="Calisto MT" Font-Size="10pt" ForeColor="#0099FF" Text="Fill It !" 
            Visible="False"></asp:Label>
        <br />
        <br />
        <br />
        &nbsp;
        <asp:Button ID="Button4" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Italic="False" Font-Names="Calisto MT" 
            Font-Size="12pt" ForeColor="#0099FF" onclick="Button4_Click" Text="Insert" 
            ToolTip="Enter Qualifications Gradute and Post Graduate Should be Related" />
        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button5" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
            ForeColor="#0099FF" onclick="Button5_Click" Text="Update" 
            ToolTip="Enter ID If You Want To Update " />
        &nbsp;&nbsp;&nbsp;<asp:Button ID="Button6" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
            ForeColor="#0099FF" onclick="Button6_Click" Text="Delete" 
            ToolTip="Write The ID When You Want To Delete" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button7" runat="server" BackColor="White" BorderColor="White" 
            BorderStyle="None" Font-Bold="True" Font-Names="Calisto MT" Font-Size="12pt" 
            ForeColor="#0099FF" onclick="Button7_Click" Text="Delete All" 
            Width="82px" />
        <br />
        <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Calisto MT" 
            Font-Size="14pt" ForeColor="#0099FF" Visible="False"></asp:Label>
        <br />
        <asp:TextBox ID="TextBox5" runat="server" Font-Names="calsito" 
            ForeColor="#0099FF" Height="39px" style="resize:none" TextMode="MultiLine" Visible="False" 
            Width="329px"></asp:TextBox>
    </asp:Panel>
    <p>
        &nbsp;<asp:Button ID="Button2" runat="server" BackColor="White" BorderColor="White" 
        BorderStyle="None" Font-Italic="False" Font-Names="Calisto MT" Font-Size="14pt" 
        ForeColor="#0099FF" Height="31px" onclick="Button2_Click" 
        Text="View The Qualification According To Company" Width="397px" />
    </p>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="SqlDataSource1" EmptyDataText="No Records" 
        Enabled="False" EnableViewState="False" Font-Bold="True" 
        Font-Names="Calisto MT" Font-Size="12pt" ForeColor="#0099FF" GridLines="None" 
        HorizontalAlign="Left" Visible="False" Width="351px" DataKeyNames="ID">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Graduate" HeaderText="Graduate" 
                SortExpression="Graduate" />
            <asp:BoundField DataField="Post_Graduate" HeaderText="Post_Graduate" 
                SortExpression="Post_Graduate" />
            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" 
                SortExpression="ID" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:recruitmentConnectionString %>" 
        EnableViewState="False" SelectCommand="SELECT * FROM [Qualification]">
    </asp:SqlDataSource>
    </form>
</body>
</html>
